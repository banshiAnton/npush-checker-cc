const ConnectyCube = require('connectycube')
const axios = require('axios').default
const sleep = require('sleep-promise')
​
const jsonConfig = require('./files/checker.json')
​
const CREDS = {
    appId: 218,
    authKey: "p-cqm2aatn8ZPM3",
    authSecret: "3hmGuXN8AHZOgg6"
}
​
const CONFIG = {
    endpoints: {
        api: jsonConfig['api'],
    },
}
​
const user = {
    login: 'pushTestUser',
    password: 'pushTestUser'
}
​
ConnectyCube.init(CREDS, CONFIG);
​
(async function main() {
    const session = await ConnectyCube.createSession(user)
    const payload = { message: 'Hello from Checker' }
    const pushParams = {
        notification_type: "push",
        user: { ids: [session.user.id] },
        message: ConnectyCube.pushnotifications.base64Encode(JSON.stringify(payload)),
    };
​
    const pushEvents = await ConnectyCube.pushnotifications.events.create(pushParams)
    const pordEvent = pushEvents.find(({ event }) => event.environment === 'production')
    //console.log('[prod event]', pordEvent)
​
    await sleep(3000) // 3 seconds delay
​
    const eventLogResponse = await axios.get(`https://${jsonConfig['api']}/events/log`, { 
        params: { event_id: pordEvent.event['_id'] },
        headers: { 'CB-Token': session['token'] } 
    })
    const eventLog = eventLogResponse.data
    //console.log('[eventLog]', eventLog)
    console.log(eventLog['delivered'])
    if (eventLog['delivered'] < 0) {
        throw new Error(JSON.stringify(eventLog))
    }
})()
​
process.addListener('unhandledRejection', error => {
    console.error(error)
    throw new Error(JSON.stringify(error))
})